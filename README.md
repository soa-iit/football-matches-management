# Football League Management Application

This Football League Management Application is a microservices-based system designed to handle various aspects of managing football leagues, teams, and related information. The application consists of the following microservices:

1. **Eureka Server**: Eureka is used as a service registry and discovery server to enable communication between microservices.

2. **Team Service**: Manages information related to football teams, including team details, players, and other relevant data.

3. **League Service**: Handles the management of football leagues, including fixtures, results, standings, and other league-related information.

4. **API Gateway**: Serves as the entry point for external clients and routes requests to the appropriate microservices.

## UML Diagrams

### League Service UML Diagram

![League Service UML Diagram](league-service-uml-diagram.png)

### Team Service UML Diagram

![Team Service UML Diagram](team-service-uml-diagram.png)

## Getting Started

To run the Football League Management Application, follow these steps:

1. Start the Eureka Server.
2. Start the API Gateway.
3. Start the Team Service.
4. Start the League Service.

Ensure that all maven dependencies are installed and properly configured before starting the services.

## API Endpoints

The API Gateway exposes endpoints for external clients to interact with the microservices. Detailed documentation for each endpoint can be found in the respective microservices' API documentation.

## Configuration

Adjust the configuration files of each microservice as needed, including database connections, port numbers, and other environment-specific settings.

## Technologies Used

- Spring Boot for microservices development
- Eureka for service registration and discovery
- API Gateway for routing and load balancing
