package tn.iit.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import tn.iit.request.CreateManagerRequest;
import tn.iit.response.ManagerResponse;
import tn.iit.service.ManagerService;

import java.util.List;

@RestController
@RequestMapping("/api/manager")
public class ManagerController {

    @Autowired
    private ManagerService managerService;

    @PostMapping("")
    public ManagerResponse createManager(@RequestBody CreateManagerRequest createManagerRequest) {
        return managerService.createManager(createManagerRequest);
    }
    @GetMapping("")
    public List<ManagerResponse> getAllManagers(){
        return managerService.getAllManagers();
    }
    @GetMapping("/{id}")
    public ManagerResponse getManagerById(@PathVariable Long id) {
        return managerService.getManagerById(id);
    }

    @PutMapping("/{id}")
    public ManagerResponse updateManager(@PathVariable Long id, @RequestBody CreateManagerRequest createManagerRequest) {
        return managerService.updateManager(id, createManagerRequest);
    }

    @DeleteMapping("/{id}")
    public List<ManagerResponse> deleteManager(@PathVariable Long id) {
        return managerService.deleteManager(id);
    }
}
