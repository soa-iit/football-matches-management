package tn.iit.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import tn.iit.request.CreateTeamRequest;
import tn.iit.response.TeamResponse;
import tn.iit.service.TeamService;

import java.util.List;

@RestController
@RequestMapping("/api/team")
public class TeamController {

    @Autowired
    private TeamService teamService;

    @PostMapping("")
    public TeamResponse createTeam(@RequestBody CreateTeamRequest createTeamRequest) {
        return teamService.createTeam(createTeamRequest);
    }

    @GetMapping("")
    public List<TeamResponse> getAllTeams() {
        return teamService.getAllTeams();
    }

    @GetMapping("/{id}")
    public TeamResponse getTeamById(@PathVariable Long id) {
        return teamService.getTeamById(id);
    }

    @PutMapping("/{id}")
    public TeamResponse updateTeam(@PathVariable Long id, @RequestBody CreateTeamRequest createTeamRequest) {
        return teamService.updateTeam(id, createTeamRequest);
    }

    @DeleteMapping("/{id}")
    public List<TeamResponse> deleteTeam(@PathVariable Long id) {
        return teamService.deleteTeam(id);
    }

    @PutMapping("/{id}/manager")
    public TeamResponse updateTeamManager(@PathVariable Long id, @RequestBody Long managerId) {
        return teamService.updateTeamManager(id, managerId);
    }
    @DeleteMapping("/{id}/manager")
    TeamResponse removeTeamManager(@PathVariable Long id){
        return teamService.removeTeamManager(id);
    }
    @PutMapping("/{id}/stadium")
    public TeamResponse updateTeamStadium(@PathVariable Long id, @RequestBody Long stadiumId) {
        return teamService.updateTeamStadium(id, stadiumId);
    }
    @DeleteMapping("/{id}/stadium")
    public TeamResponse removeTeamStadium(@PathVariable Long id){
        return teamService.removeTeamStadium(id);
    }
    @PutMapping("/{id}/players")
    public TeamResponse addPlayerToTeam(@PathVariable Long id, @RequestBody Long playerId) {
        return teamService.addPlayerToTeam(id, playerId);
    }
    @DeleteMapping("/{id}/players")
    public TeamResponse removePlayerFromTeam(@PathVariable Long id, @RequestBody Long playerId){
        return teamService.removePlayerFromTeam(id, playerId);
    }

}
