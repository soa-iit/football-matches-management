package tn.iit.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.*;

import java.io.Serializable;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@EqualsAndHashCode(onlyExplicitlyIncluded = true)

@Entity
@Table(name = "teams")
public class Team implements Serializable {

    @ToString.Exclude
    @JsonIgnore
    @OneToOne
    @JoinColumn(name = "id_manager")
    Manager manager;
    @ToString.Exclude
    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "id_stadium")
    Stadium stadium;
    @ToString.Exclude
    @JsonIgnore
    @OneToMany(mappedBy = "team")
    private List<Player> players;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;

    public Team(Long id, String name) {
        this.id = id;
        this.name = name;
    }
}
