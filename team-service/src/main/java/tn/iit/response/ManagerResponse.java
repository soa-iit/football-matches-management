package tn.iit.response;

import lombok.Getter;
import lombok.Setter;
import tn.iit.entity.Manager;
import tn.iit.entity.Team;

@Getter
@Setter
public class ManagerResponse {
    private Long id;
    private String fullName;
    private Team team;

    public ManagerResponse(Manager manager){
        this.id = manager.getId();
        this.fullName = manager.getFirstName()+" "+manager.getLastName();
        this.team = manager.getTeam();
    }
}
