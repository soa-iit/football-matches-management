package tn.iit.response;

import lombok.Getter;
import lombok.Setter;
import tn.iit.entity.Manager;
import tn.iit.entity.Player;
import tn.iit.entity.Stadium;
import tn.iit.entity.Team;

import java.util.List;

@Setter
@Getter
public class TeamResponse {

    private Long id;
    private String name;
    private Stadium stadium;
    private Manager manager;
    private List<Player> players;

    public TeamResponse(Team team) {
        this.id = team.getId();
        this.name = team.getName();
        this.stadium = team.getStadium();
        this.manager = team.getManager();
        this.players = team.getPlayers();
    }
}
