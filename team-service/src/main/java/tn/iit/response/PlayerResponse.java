package tn.iit.response;

import lombok.Getter;
import lombok.Setter;
import tn.iit.entity.Player;
import tn.iit.entity.Position;
import tn.iit.entity.Team;

@Setter
@Getter
public class PlayerResponse {
    private Long id;
    private String fullName;
    private String nationality;
    private Position position;
    private Team team;

    public PlayerResponse(Player player) {
        this.id = player.getId();
        this.fullName = player.getFirstName() + " " + player.getLastName();
        this.nationality = player.getNationality();
        this.position = player.getPosition();
        this.team = player.getTeam();
    }
}
