package tn.iit.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import tn.iit.entity.Manager;

@Repository
public interface ManagerRepository extends JpaRepository<Manager, Long> {
    @Modifying
    @Query("UPDATE Team t SET t.manager = null WHERE t.manager = :manager")
    void removeManagerAssociation(@Param("manager") Manager manager);
}
