package tn.iit.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import tn.iit.entity.Team;

@Repository
public interface TeamRepository extends JpaRepository<Team, Long> {
    @Modifying
    @Query("UPDATE Player p SET p.team = null WHERE p.team = :team")
    void removeTeamPlayerAssociation(@Param("team") Team team);

    @Modifying
    @Query("UPDATE Manager m SET m.team = null WHERE m.team = :team")
    void removeTeamManagerAssociation(@Param("team") Team team);
}
