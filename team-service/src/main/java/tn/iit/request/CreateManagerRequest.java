package tn.iit.request;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class CreateManagerRequest {
    private String firstName;
    private String lastName;
}
