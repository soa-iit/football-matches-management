package tn.iit.service;

import jakarta.persistence.EntityNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tn.iit.entity.Manager;
import tn.iit.entity.Player;
import tn.iit.entity.Stadium;
import tn.iit.entity.Team;
import tn.iit.repository.ManagerRepository;
import tn.iit.repository.PlayerRepository;
import tn.iit.repository.StadiumRepository;
import tn.iit.repository.TeamRepository;
import tn.iit.request.CreateTeamRequest;
import tn.iit.response.ManagerResponse;
import tn.iit.response.PlayerResponse;
import tn.iit.response.TeamResponse;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class TeamService {

    @Autowired
    private TeamRepository teamRepository;
    @Autowired
    private ManagerRepository managerRepository;
    @Autowired
    private StadiumRepository stadiumRepository;
    @Autowired
    private PlayerRepository playerRepository;

    public TeamResponse createTeam(CreateTeamRequest createTeamRequest) {
        Team team = new Team();
        team.setName(createTeamRequest.getName());
        team = teamRepository.save(team);
        return new TeamResponse(team);
    }
    public List<TeamResponse> getAllTeams(){
        List<Team> teamList = teamRepository.findAll();
        return teamList.stream().map(TeamResponse::new).collect(Collectors.toList());
    }
    public TeamResponse getTeamById(Long id) {
        Team team = teamRepository.findById(id).get();
        return new TeamResponse(team);
    }
    public TeamResponse updateTeam(Long id, CreateTeamRequest createTeamRequest){
        Optional<Team> teamOptional = teamRepository.findById(id);
        if(teamOptional.isPresent()){
            Team team = teamOptional.get();
            team.setName(createTeamRequest.getName());
            team = teamRepository.save(team);
            return new TeamResponse(team);
        } else {
            throw new EntityNotFoundException("Team with ID "+id+" not found.");
        }
    }
    @Transactional
    public List<TeamResponse> deleteTeam(Long id){
        Optional<Team> teamOptional = teamRepository.findById(id);
        if(teamOptional.isPresent()){
            Team team = teamOptional.get();
            teamRepository.removeTeamManagerAssociation(team);
            teamRepository.removeTeamPlayerAssociation(team);
        }
        teamRepository.deleteById(id);
        return getAllTeams();
    }
    public TeamResponse updateTeamStadium(Long teamId, Long stadiumId) {
        Stadium stadium = stadiumRepository.findById(stadiumId).get();
        Team team = teamRepository.findById(teamId).get();
        team.setStadium(stadium);
        team = teamRepository.save(team);
        return new TeamResponse(team);
    }
    public TeamResponse updateTeamManager(Long teamId, Long managerId) {
        Manager manager = managerRepository.findById(managerId).get();
        Team team = teamRepository.findById(teamId).get();
        team.setManager(manager);
        manager.setTeam(team);
        managerRepository.save(manager);
        team = teamRepository.save(team);
        return new TeamResponse(team);
    }
    public TeamResponse addPlayerToTeam(Long id, Long playerId) {
        Team team = teamRepository.findById(id).get();
        Player player = playerRepository.findById(playerId).get();
        player.setTeam(team);
        player = playerRepository.save(player);
        return new TeamResponse(player.getTeam());
    }
    public TeamResponse removeTeamStadium(Long id){
        Optional<Team> teamOptional = teamRepository.findById(id);
        if(teamOptional.isPresent()) {
            Team team = teamOptional.get();
            team.setStadium(null);
            team = teamRepository.save(team);
            return new TeamResponse(team);
        } else {
            throw new EntityNotFoundException("Team with ID "+id+" not found.");
        }
    }
    public TeamResponse removeTeamManager(Long id){
        Optional<Team> teamOptional = teamRepository.findById(id);
        if(teamOptional.isPresent()) {
            Team team = teamOptional.get();
            Manager manager = team.getManager();
            if(team.getManager()!=null) {
                manager.setTeam(null);
                managerRepository.save(manager);
            }
            team.setManager(null);
            team = teamRepository.save(team);
            return new TeamResponse(team);
        } else {
            throw new EntityNotFoundException("Team with ID "+id+" not found.");
        }
    }
    public TeamResponse removePlayerFromTeam(Long id, Long playerId){
        Optional<Player> playerOptional = playerRepository.findById(playerId);
        if(playerOptional.isPresent()){
            Player player = playerOptional.get();
            player.setTeam(null);
            playerRepository.save(player);
        } else {
            throw new EntityNotFoundException("Player with ID "+id+ " not found.");
        }
        return getTeamById(id);
    }
}
