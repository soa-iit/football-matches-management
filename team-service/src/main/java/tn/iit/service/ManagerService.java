package tn.iit.service;

import jakarta.persistence.EntityNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tn.iit.entity.Manager;
import tn.iit.repository.ManagerRepository;
import tn.iit.request.CreateManagerRequest;
import tn.iit.response.ManagerResponse;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class ManagerService {

    @Autowired
    private ManagerRepository managerRepository;

    public ManagerResponse createManager(CreateManagerRequest createManagerRequest) {
        Manager manager = new Manager();
        manager.setFirstName(createManagerRequest.getFirstName());
        manager.setLastName(createManagerRequest.getLastName());
        manager = managerRepository.save(manager);
        return new ManagerResponse(manager);
    }

    public ManagerResponse getManagerById(Long id) {
        return managerRepository.findById(id).map(ManagerResponse::new).orElseThrow(() -> new EntityNotFoundException("Stadium not found with id: " + id));
    }

    public List<ManagerResponse> getAllManagers() {
        List<Manager> managers = managerRepository.findAll();
        return managers.stream().map(ManagerResponse::new).collect(Collectors.toList());
    }

    public ManagerResponse updateManager(Long id, CreateManagerRequest createManagerRequest) {
        Optional<Manager> optionalManager = managerRepository.findById(id);
        if (optionalManager.isPresent()) {
            Manager manager = optionalManager.get();
            manager.setFirstName(createManagerRequest.getFirstName());
            manager.setLastName(createManagerRequest.getLastName());
            manager = managerRepository.save(manager);
            return new ManagerResponse(manager);
        } else {
            throw new EntityNotFoundException("Manager with ID " + id + " not found");
        }
    }


    @Transactional
    public List<ManagerResponse> deleteManager(Long id) {
        Optional<Manager> optionalManager = managerRepository.findById(id);
        if (optionalManager.isPresent()) {
            Manager manager = optionalManager.get();
            managerRepository.removeManagerAssociation(manager);
        }
        managerRepository.deleteById(id);
        return getAllManagers();
    }
}
