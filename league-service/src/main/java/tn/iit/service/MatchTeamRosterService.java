package tn.iit.service;

import jakarta.persistence.EntityNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tn.iit.entity.MatchTeamRoster;
import tn.iit.entity.PlayerId;
import tn.iit.entity.Substitution;
import tn.iit.proxy.TeamServiceFeignClient;
import tn.iit.repository.MatchTeamRosterRepository;
import tn.iit.repository.PlayerIdRepository;
import tn.iit.repository.SubstitutionRepository;
import tn.iit.request.CreateSubstitutionRequest;
import tn.iit.request.SetMatchTeamRosterRequest;
import tn.iit.response.MatchTeamRosterResponse;
import tn.iit.response.PlayerResponse;
import tn.iit.response.SubstitutionResponse;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Service
public class MatchTeamRosterService {

    @Autowired
    private PlayerIdRepository playerIdRepository;

    @Autowired
    private MatchTeamRosterRepository matchTeamRosterRepository;

    @Autowired
    private SubstitutionRepository substitutionRepository;

    @Autowired
    private TeamServiceFeignClient teamServiceFeignClient;

    public MatchTeamRosterResponse getMatchTeamRosterById(Long id) {
        MatchTeamRoster matchTeamRoster = matchTeamRosterRepository.findById(id).orElseThrow(() -> new EntityNotFoundException("Match Team Roster with ID " + id + " not found."));
        List<PlayerResponse> playerResponseList = matchTeamRoster.getPlayerIds().stream().map(playerId -> teamServiceFeignClient.getPlayerById(playerId.getPlayerId())).toList();
        return new MatchTeamRosterResponse(matchTeamRoster, playerResponseList);
    }

    @Transactional
    public MatchTeamRosterResponse setMatchTeamRoster(Long id, SetMatchTeamRosterRequest setMatchTeamRosterRequest) {
        MatchTeamRoster matchTeamRoster = matchTeamRosterRepository.findById(id).orElseThrow(() -> new EntityNotFoundException("Match Team Roster with ID " + id + " not found."));
        List<PlayerResponse> playerResponseList = new ArrayList<>();
        playerIdRepository.deletePlayerIdByMatchTeamRoster(matchTeamRoster);
        MatchTeamRoster finalMatchTeamRoster = matchTeamRoster;
        List<PlayerId> newPlayerIds = setMatchTeamRosterRequest.getPlayerIds().stream()
                .map(playerIdValue -> {
                    PlayerId playerId = new PlayerId();
                    playerId.setMatchTeamRoster(finalMatchTeamRoster);
                    playerId.setPlayerId(playerIdValue);
                    PlayerResponse playerResponse = teamServiceFeignClient.getPlayerById(playerIdValue);
                    if (checkPlayerBelongsToTeam(playerResponse.getTeam().getId(), finalMatchTeamRoster.getTeamId()))
                        playerResponseList.add(teamServiceFeignClient.getPlayerById(playerIdValue));
                    return playerId;
                })
                .toList();
        newPlayerIds = playerIdRepository.saveAll(newPlayerIds);
        matchTeamRoster.setPlayerIds(newPlayerIds);
        matchTeamRoster = matchTeamRosterRepository.save(matchTeamRoster);
        return new MatchTeamRosterResponse(matchTeamRoster, playerResponseList);
    }

    public SubstitutionResponse addSubstitutionToMatchTeamRoster(Long id, CreateSubstitutionRequest createSubstitutionRequest) {
        MatchTeamRoster matchTeamRoster = matchTeamRosterRepository.findById(id).orElseThrow(() -> new EntityNotFoundException("Match Team Roster with ID " + id + " not found."));
        PlayerResponse playerInResponse = teamServiceFeignClient.getPlayerById(createSubstitutionRequest.getPlayerIn());
        PlayerResponse playerOutResponse = teamServiceFeignClient.getPlayerById(createSubstitutionRequest.getPlayerOut());
        checkPlayerBelongsToTeam(playerInResponse.getTeam().getId(), matchTeamRoster.getTeamId());
        checkPlayerBelongsToTeam(playerOutResponse.getTeam().getId(), matchTeamRoster.getTeamId());
        Substitution substitution = new Substitution();
        substitution.setMatchTeamRoster(matchTeamRoster);
        substitution.setPlayerIn(createSubstitutionRequest.getPlayerIn());
        substitution.setPlayerOut(createSubstitutionRequest.getPlayerOut());
        substitution.setSubstitutionTime(createSubstitutionRequest.getSubstitutionTime());
        substitution = substitutionRepository.save(substitution);
        return new SubstitutionResponse(substitution, playerInResponse, playerOutResponse);
    }

    public List<SubstitutionResponse> getAllMatchTeamRosterSubstitutions(Long id) {
        MatchTeamRoster matchTeamRoster = matchTeamRosterRepository.findById(id).orElseThrow(() -> new EntityNotFoundException("Match Team Roster with ID " + id + " not found."));
        List<Substitution> substitutions = matchTeamRoster.getSubstitutions();
        return substitutions.stream().map(substitution -> {
            PlayerResponse playerInResponse = teamServiceFeignClient.getPlayerById(substitution.getPlayerIn());
            PlayerResponse playerOutResponse = teamServiceFeignClient.getPlayerById(substitution.getPlayerOut());
            return new SubstitutionResponse(substitution, playerInResponse, playerOutResponse);

        }).toList();
    }

    private boolean checkPlayerBelongsToTeam(Long playerTeamId, Long rosterTeamId) {
        if (!Objects.equals(playerTeamId, rosterTeamId))
            throw new EntityNotFoundException("Player doesn't belong to team.");
        return true;
    }
}
