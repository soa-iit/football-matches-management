package tn.iit.service;

import jakarta.persistence.EntityNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tn.iit.entity.Goal;
import tn.iit.entity.Match;
import tn.iit.entity.Referee;
import tn.iit.proxy.TeamServiceFeignClient;
import tn.iit.repository.GoalRepository;
import tn.iit.repository.MatchRepository;
import tn.iit.repository.PlayerIdRepository;
import tn.iit.repository.RefereeRepository;
import tn.iit.request.CreateGoalRequest;
import tn.iit.request.CreateMatchRequest;
import tn.iit.response.*;

import java.util.Objects;
import java.util.Optional;

@Service
public class MatchService {

    @Autowired
    private MatchRepository matchRepository;

    @Autowired
    private RefereeRepository refereeRepository;

    @Autowired
    private PlayerIdRepository playerIdRepository;

    @Autowired
    private GoalRepository goalRepository;

    @Autowired
    private TeamServiceFeignClient teamServiceFeignClient;

    public MatchResponse getMatchById(Long id) {
        Optional<Match> matchOptional = matchRepository.findById(id);
        if (matchOptional.isPresent()) {
            Match match = matchOptional.get();
            TeamResponse hostTeam = teamServiceFeignClient.getTeamById(match.getHostTeamRoster().getTeamId());
            TeamResponse visitorTeam = teamServiceFeignClient.getTeamById(match.getVisitorTeamRoster().getTeamId());
            return new MatchResponse(match, hostTeam, visitorTeam);
        } else {
            throw new EntityNotFoundException("Match with ID " + id + " not found.");
        }
    }

    @Transactional
    public MatchResponse updateMatch(Long id, CreateMatchRequest createMatchRequest) {
        Optional<Match> matchOptional = matchRepository.findById(id);
        if (matchOptional.isPresent()) {
            Match match = matchOptional.get();
            if (!Objects.equals(match.getHostTeamRoster().getTeamId(), createMatchRequest.getHostTeamId())) {
                playerIdRepository.deletePlayerIdByMatchTeamRoster(match.getHostTeamRoster());
                match.getHostTeamRoster().setTeamId(createMatchRequest.getHostTeamId());
            }
            if (!Objects.equals(match.getVisitorTeamRoster().getTeamId(), createMatchRequest.getVisitorTeamId())) {
                playerIdRepository.deletePlayerIdByMatchTeamRoster(match.getVisitorTeamRoster());
                match.getVisitorTeamRoster().setTeamId(createMatchRequest.getVisitorTeamId());
            }
            match.setMatchDate(createMatchRequest.getMatchDate());
            match.setTotalSpectators(createMatchRequest.getTotalSpectators());
            matchRepository.save(match);
            return getMatchById(id);
        } else {
            throw new EntityNotFoundException("Match with ID " + id + " not found.");
        }
    }

    public MatchResponse updateMatchReferee(Long id, Long refereeId) {
        Optional<Match> matchOptional = matchRepository.findById(id);
        Optional<Referee> refereeOptional = refereeRepository.findById(refereeId);
        if (matchOptional.isPresent() && refereeOptional.isPresent()) {
            Match match = matchOptional.get();
            Referee referee = refereeOptional.get();
            match.setReferee(referee);
            matchRepository.save(match);
            return getMatchById(id);
        } else {
            throw new EntityNotFoundException("Match or Referee ID not found.");
        }
    }

    public MatchResponse deleteMatch(Long id) {
        Optional<Match> matchOptional = matchRepository.findById(id);
        if (matchOptional.isPresent()) {
            MatchResponse matchResponse = getMatchById(matchOptional.get().getId());
            matchRepository.deleteById(id);
            return matchResponse;
        } else {
            throw new EntityNotFoundException("Match with ID " + id + " not found.");
        }
    }

    public MatchResponse addGoalToMatch(Long id, CreateGoalRequest createGoalRequest){
        Match match = matchRepository.findById(id).orElseThrow(() -> new EntityNotFoundException("Match with ID " + id + " not found."));
        Goal goal = new Goal();
        goal.setPlayerId(createGoalRequest.getPlayerId());
        goal.setOwnGoal(createGoalRequest.isOwnGoal());
        goal.setGoalTime(createGoalRequest.getGoalTime());
        goal.setMatch(match);
        goalRepository.save(goal);
        return getMatchById(id);
    }

    public MatchResultResponse getMatchResult(Long id) {
        Match match = matchRepository.findById(id).orElseThrow(() -> new EntityNotFoundException("Match with ID " + id + " not found."));
        int hostTeamGoals, visistorTeamGoals = 0;
        hostTeamGoals = match.getGoals().stream()
                .map(goal -> {
                    PlayerResponse scorer = teamServiceFeignClient.getPlayerById(goal.getPlayerId());
                    return (Objects.equals(scorer.getTeam().getId(), match.getHostTeamRoster().getTeamId()) && !goal.isOwnGoal()) ? 1 : 0;
                })
                .filter(goal -> goal == 1)
                .mapToInt(Integer::intValue)
                .sum();
        visistorTeamGoals = match.getGoals().size()-hostTeamGoals;
        TeamResponse hostTeam = teamServiceFeignClient.getTeamById(match.getHostTeamRoster().getTeamId());
        TeamResponse visitorTeam = teamServiceFeignClient.getTeamById(match.getVisitorTeamRoster().getTeamId());
        return new MatchResultResponse(hostTeam, visitorTeam, hostTeamGoals, visistorTeamGoals);
    }
}
