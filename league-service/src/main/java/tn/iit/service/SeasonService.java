package tn.iit.service;

import jakarta.persistence.EntityNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tn.iit.entity.Match;
import tn.iit.entity.MatchDay;
import tn.iit.entity.Season;
import tn.iit.proxy.TeamServiceFeignClient;
import tn.iit.repository.MatchDayRepository;
import tn.iit.repository.SeasonRepository;
import tn.iit.request.CreateSeasonRequest;
import tn.iit.response.MatchResultResponse;
import tn.iit.response.RankingResponse;
import tn.iit.response.SeasonResponse;
import tn.iit.response.TeamResponse;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class SeasonService {
    @Autowired
    private SeasonRepository seasonRepository;
    @Autowired
    private MatchDayRepository matchDayRepository;
    @Autowired
    private MatchService matchService;
    @Autowired
    private TeamServiceFeignClient teamServiceFeignClient;

    public SeasonResponse createSeason(CreateSeasonRequest createSeasonRequest) {
        Season season = new Season();
        season.setStartYear(createSeasonRequest.getStartYear());
        season.setEndYear(createSeasonRequest.getEndYear());
        season = seasonRepository.save(season);
        return new SeasonResponse(season);
    }

    public List<SeasonResponse> getAllSeason() {
        List<Season> seasonList = seasonRepository.findAll();
        return seasonList.stream().map(SeasonResponse::new).collect(Collectors.toList());
    }

    public SeasonResponse getSeasonById(Long id) {
        Optional<Season> seasonOptional = seasonRepository.findById(id);
        if (seasonOptional.isPresent()) {
            Season season = seasonOptional.get();
            return new SeasonResponse(season);
        } else {
            throw new EntityNotFoundException("Season with ID " + id + " not found.");
        }
    }

    public SeasonResponse updateSeason(Long id, CreateSeasonRequest createSeasonRequest) {
        Optional<Season> seasonOptional = seasonRepository.findById(id);
        if (seasonOptional.isPresent()) {
            Season season = seasonOptional.get();
            season.setStartYear(createSeasonRequest.getStartYear());
            season.setEndYear(createSeasonRequest.getEndYear());
            season = seasonRepository.save(season);
            return new SeasonResponse(season);
        } else {
            throw new EntityNotFoundException("Season with ID " + id + " not found.");
        }
    }

    public List<SeasonResponse> deleteSeason(Long id) {
        seasonRepository.deleteById(id);
        return getAllSeason();
    }

    public SeasonResponse addMatchDay(Long id) {
        Optional<Season> seasonOptional = seasonRepository.findById(id);
        if (seasonOptional.isPresent()) {
            Season season = seasonOptional.get();
            MatchDay matchDay = new MatchDay();
            matchDay.setSeason(season);
            matchDay.setMatchDayNumber(season.getMatchDays().size() + 1);
            matchDayRepository.save(matchDay);
            season.getMatchDays().add(matchDay);
            season = seasonRepository.save(season);
            return new SeasonResponse(season);
        } else {
            throw new EntityNotFoundException("Season with ID " + id + " not found.");
        }
    }

    public SeasonResponse deleteMatchDay(Long id, Long matchDayId) {
        Optional<MatchDay> matchDayOptional = matchDayRepository.findById(matchDayId);
        if (matchDayOptional.isPresent()) {
            matchDayRepository.deleteById(matchDayId);
            return getSeasonById(id);
        } else {
            throw new EntityNotFoundException("Match Day with ID " + matchDayId + " not found.");
        }
    }

    public List<RankingResponse> getSeasonStandings(Long id){
        Season season = seasonRepository.findById(id).orElseThrow(()->new EntityNotFoundException("Season with ID "+id+ " not found."));
        List<MatchDay> matchDays = season.getMatchDays();
        HashMap<Long, Integer> teamsTotalPoints = new HashMap<>();
        for (MatchDay matchDay: matchDays) {
            List<Match> matches = matchDay.getMatches();
            for (Match match : matches){
                MatchResultResponse matchResultResponse = matchService.getMatchResult(match.getId());
                Long hostTeamId = match.getHostTeamRoster().getTeamId();
                Long visitorTeamId = match.getVisitorTeamRoster().getTeamId();
                if(matchResultResponse.getHostTeamGoals()>matchResultResponse.getVisitorTeamGoals()) {
                    teamsTotalPoints.put(hostTeamId, teamsTotalPoints.getOrDefault(hostTeamId, 0)+3);
                    teamsTotalPoints.put(visitorTeamId, teamsTotalPoints.getOrDefault(visitorTeamId, 0));
                } else if (matchResultResponse.getHostTeamGoals()<matchResultResponse.getVisitorTeamGoals()){
                    teamsTotalPoints.put(hostTeamId, teamsTotalPoints.getOrDefault(hostTeamId, 0));
                    teamsTotalPoints.put(visitorTeamId, teamsTotalPoints.getOrDefault(visitorTeamId, 0)+3);
                } else {
                    teamsTotalPoints.put(hostTeamId, teamsTotalPoints.getOrDefault(hostTeamId, 0)+1);
                    teamsTotalPoints.put(visitorTeamId, teamsTotalPoints.getOrDefault(visitorTeamId, 0)+1);
                }
            }
        }
        List<Map.Entry<Long, Integer>> entryList = new ArrayList<>(teamsTotalPoints.entrySet());

        entryList.sort(Map.Entry.comparingByValue(Comparator.reverseOrder()));

        List<RankingResponse> rankings = new ArrayList<>();
        entryList.forEach(entry -> {
            TeamResponse teamResponse = teamServiceFeignClient.getTeamById(entry.getKey());
            rankings.add(new RankingResponse(teamResponse, entry.getValue()));
        });
        return rankings;
    }
}
