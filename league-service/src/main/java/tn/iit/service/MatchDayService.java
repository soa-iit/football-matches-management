package tn.iit.service;

import jakarta.persistence.EntityNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tn.iit.entity.Match;
import tn.iit.entity.MatchDay;
import tn.iit.entity.MatchTeamRoster;
import tn.iit.proxy.TeamServiceFeignClient;
import tn.iit.repository.MatchDayRepository;
import tn.iit.repository.MatchRepository;
import tn.iit.repository.MatchTeamRosterRepository;
import tn.iit.request.CreateMatchRequest;
import tn.iit.response.MatchDayResponse;
import tn.iit.response.TeamResponse;

import java.util.Optional;

@Service
public class MatchDayService {

    @Autowired
    private MatchDayRepository matchDayRepository;

    @Autowired
    private MatchRepository matchRepository;

    @Autowired
    private MatchTeamRosterRepository matchTeamRosterRepository;

    @Autowired
    private TeamServiceFeignClient teamServiceFeignClient;

    public MatchDayResponse addMatch(Long id, CreateMatchRequest createMatchRequest){
        Optional<MatchDay> matchDayOptional = matchDayRepository.findById(id);
        if(matchDayOptional.isPresent()){
            MatchDay matchDay =matchDayOptional.get();
            TeamResponse hostTeamResponse = teamServiceFeignClient.getTeamById(createMatchRequest.getHostTeamId());
            TeamResponse visitorTeamResponse = teamServiceFeignClient.getTeamById(createMatchRequest.getVisitorTeamId());
            Match match = new Match();
            match.setMatchDay(matchDay);
            match.setMatchDate(createMatchRequest.getMatchDate());
            match = matchRepository.save(match);
            MatchTeamRoster hostMatchTeamRoster = new MatchTeamRoster();
            MatchTeamRoster visitorMatchTeamRoster = new MatchTeamRoster();
            hostMatchTeamRoster.setMatch(match);
            hostMatchTeamRoster.setTeamId(hostTeamResponse.getId());
            visitorMatchTeamRoster.setMatch(match);
            visitorMatchTeamRoster.setTeamId(visitorTeamResponse.getId());
            matchTeamRosterRepository.save(hostMatchTeamRoster);
            matchTeamRosterRepository.save(visitorMatchTeamRoster);
            match.setHostTeamRoster(hostMatchTeamRoster);
            match.setVisitorTeamRoster(visitorMatchTeamRoster);
            match = matchRepository.save(match);
            matchDay.getMatches().add(match);
            matchDay = matchDayRepository.save(matchDay);
            return new MatchDayResponse(matchDay);
        } else {
            throw new EntityNotFoundException("Match Day with ID "+id+" not found.");
        }
    }
}
