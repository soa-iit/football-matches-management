package tn.iit.service;

import jakarta.persistence.EntityNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tn.iit.entity.Referee;
import tn.iit.repository.RefereeRepository;
import tn.iit.request.CreateRefereeRequest;
import tn.iit.response.RefereeResponse;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class RefereeService {

    @Autowired
    private RefereeRepository refereeRepository;

    public RefereeResponse createReferee(CreateRefereeRequest createRefereeRequest) {
        Referee referee = new Referee();
        referee.setFirstName(createRefereeRequest.getFirstName());
        referee.setLastName(createRefereeRequest.getLastName());
        referee = refereeRepository.save(referee);
        return new RefereeResponse(referee);
    }

    public List<RefereeResponse> getAllReferees() {
        List<Referee> refereeList = refereeRepository.findAll();
        return refereeList.stream().map(RefereeResponse::new).collect(Collectors.toList());
    }

    public RefereeResponse getRefereeById(Long id ){
        Optional<Referee> refereeOptional = refereeRepository.findById(id);
        if (refereeOptional.isPresent()) {
            Referee referee = refereeOptional.get();
            return new RefereeResponse(referee);
        } else {
            throw new EntityNotFoundException("Referee with ID "+id+" not found.");
        }
    }
    public RefereeResponse updateReferee(Long id, CreateRefereeRequest createRefereeRequest) {
        Optional<Referee> refereeOptional = refereeRepository.findById(id);
        if (refereeOptional.isPresent()) {
            Referee referee = refereeOptional.get();
            referee.setFirstName(createRefereeRequest.getFirstName());
            referee.setLastName(createRefereeRequest.getLastName());
            referee = refereeRepository.save(referee);
            return new RefereeResponse(referee);
        } else {
            throw new EntityNotFoundException("Referee with ID " + id + " not found.");
        }
    }

}
