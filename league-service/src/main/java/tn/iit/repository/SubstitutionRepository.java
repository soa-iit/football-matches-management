package tn.iit.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import tn.iit.entity.Substitution;

@Repository
public interface SubstitutionRepository extends JpaRepository<Substitution, Long> {
}
