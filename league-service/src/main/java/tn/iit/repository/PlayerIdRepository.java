package tn.iit.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import tn.iit.entity.MatchTeamRoster;
import tn.iit.entity.PlayerId;

@Repository
public interface PlayerIdRepository extends JpaRepository<PlayerId, Long> {
    @Modifying
    @Query("DELETE FROM PlayerId p WHERE p.matchTeamRoster= :matchTeamRoster")
    void deletePlayerIdByMatchTeamRoster(@Param("matchTeamRoster") MatchTeamRoster matchTeamRoster);
}
