package tn.iit.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import tn.iit.entity.MatchDay;
import tn.iit.entity.MatchTeamRoster;

@Repository
public interface MatchTeamRosterRepository extends JpaRepository<MatchTeamRoster, Long> {
}
