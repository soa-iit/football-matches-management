package tn.iit.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import tn.iit.entity.Goal;

@Repository
public interface GoalRepository extends JpaRepository<Goal, Long> {
}
