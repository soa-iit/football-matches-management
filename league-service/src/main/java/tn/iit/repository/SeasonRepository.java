package tn.iit.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import tn.iit.entity.Season;

@Repository
public interface SeasonRepository extends JpaRepository<Season, Long> {
}
