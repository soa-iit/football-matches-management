package tn.iit.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import tn.iit.entity.MatchDay;

@Repository
public interface MatchDayRepository extends JpaRepository<MatchDay, Long> {
}
