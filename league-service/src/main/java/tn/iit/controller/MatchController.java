package tn.iit.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import tn.iit.request.CreateGoalRequest;
import tn.iit.request.CreateMatchRequest;
import tn.iit.response.MatchResponse;
import tn.iit.response.MatchResultResponse;
import tn.iit.service.MatchService;

@RestController
@RequestMapping("/api/match")
public class MatchController {

    @Autowired
    private MatchService matchService;

    @GetMapping("/{id}")
    public MatchResponse getMatchById(@PathVariable Long id) {
        return matchService.getMatchById(id);
    }

    @PutMapping("/{id}")
    public MatchResponse updateMatch(@PathVariable Long id, @RequestBody CreateMatchRequest createMatchRequest) {
        return matchService.updateMatch(id, createMatchRequest);
    }

    @DeleteMapping("/{id}")
    public MatchResponse deleteMatch(@PathVariable Long id) {
        return matchService.deleteMatch(id);
    }

    @PutMapping("/{id}/referee")
    public MatchResponse updateMatchReferee(@PathVariable Long id, @RequestBody Long refereeId) {
        return matchService.updateMatchReferee(id, refereeId);
    }
    @PutMapping("/{id}/goal")
    public MatchResponse addGoalToMatch(@PathVariable Long id, @RequestBody CreateGoalRequest createGoalRequest){
        return matchService.addGoalToMatch(id, createGoalRequest);
    }
    @GetMapping("/{id}/result")
    public MatchResultResponse getMatchResult(@PathVariable Long id){
        return matchService.getMatchResult(id);
    }

}
