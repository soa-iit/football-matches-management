package tn.iit.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import tn.iit.request.CreateSeasonRequest;
import tn.iit.response.RankingResponse;
import tn.iit.response.SeasonResponse;
import tn.iit.service.SeasonService;

import java.util.List;

@RestController
@RequestMapping("/api/season")
public class SeasonController {

    @Autowired
    private SeasonService seasonService;

    @PostMapping("")
    public SeasonResponse createSeason(@RequestBody CreateSeasonRequest createSeasonRequest) {
        return seasonService.createSeason(createSeasonRequest);
    }

    @GetMapping("")
    public List<SeasonResponse> getAllSeasons() {
        return seasonService.getAllSeason();
    }

    @GetMapping("/{id}")
    public SeasonResponse getSeasonById(@PathVariable Long id) {
        return seasonService.getSeasonById(id);
    }

    @PutMapping("/{id}")
    public SeasonResponse updateSeason(@PathVariable Long id, @RequestBody CreateSeasonRequest createSeasonRequest) {
        return seasonService.updateSeason(id, createSeasonRequest);
    }

    @DeleteMapping("/{id}")
    public List<SeasonResponse> deleteSeason(@PathVariable Long id) {
        return seasonService.deleteSeason(id);
    }

    @PostMapping("/{id}/matchday")
    public SeasonResponse addMatchDay(@PathVariable Long id) {
        return seasonService.addMatchDay(id);
    }

    @DeleteMapping("/{id}/matchday")
    public SeasonResponse deleteMatchDay(@PathVariable Long id, @RequestBody Long matchDayId) {
        return seasonService.deleteMatchDay(id, matchDayId);
    }

    @GetMapping("/{id}/standings")
    public List<RankingResponse> getSeasonStandings(@PathVariable Long id){
        return seasonService.getSeasonStandings(id);
    }
}
