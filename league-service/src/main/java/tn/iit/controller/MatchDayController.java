package tn.iit.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import tn.iit.request.CreateMatchRequest;
import tn.iit.response.MatchDayResponse;
import tn.iit.service.MatchDayService;

@RestController
@RequestMapping("/api/match-day")
public class MatchDayController {

    @Autowired
    private MatchDayService matchDayService;

    @PostMapping("/{id}")
    public MatchDayResponse addMatch(@PathVariable Long id, @RequestBody CreateMatchRequest createMatchRequest){
        return matchDayService.addMatch(id, createMatchRequest);
    }
}
