package tn.iit.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import tn.iit.request.CreateRefereeRequest;
import tn.iit.response.RefereeResponse;
import tn.iit.service.RefereeService;

import java.util.List;

@RestController
@RequestMapping("/api/referee")
public class RefereeController {

    @Autowired
    private RefereeService refereeService;

    @PostMapping("")
    public RefereeResponse createReferee(@RequestBody CreateRefereeRequest createRefereeRequest) {
        return refereeService.createReferee(createRefereeRequest);
    }

    @GetMapping("")
    public List<RefereeResponse> getAllReferees() {
        return refereeService.getAllReferees();
    }

    @GetMapping("/{id}")
    public RefereeResponse getRefereeById(@PathVariable Long id) {
        return refereeService.getRefereeById(id);
    }

    @PutMapping("/{id}")
    public RefereeResponse updateReferee(@PathVariable Long id, @RequestBody CreateRefereeRequest createRefereeRequest) {
        return refereeService.updateReferee(id, createRefereeRequest);
    }
}
