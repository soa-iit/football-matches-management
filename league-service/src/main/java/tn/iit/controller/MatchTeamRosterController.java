package tn.iit.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import tn.iit.request.CreateSubstitutionRequest;
import tn.iit.request.SetMatchTeamRosterRequest;
import tn.iit.response.MatchTeamRosterResponse;
import tn.iit.response.SubstitutionResponse;
import tn.iit.service.MatchTeamRosterService;

import java.util.List;

@RestController
@RequestMapping("/api/match-team-roster")
public class MatchTeamRosterController {

    @Autowired
    private MatchTeamRosterService matchTeamRosterService;

    @PutMapping("/{id}")
    public MatchTeamRosterResponse setMatchTeamRoster(@PathVariable Long id, @RequestBody SetMatchTeamRosterRequest setMatchTeamRosterRequest) {
        return matchTeamRosterService.setMatchTeamRoster(id, setMatchTeamRosterRequest);
    }
    @GetMapping("/{id}")
    public MatchTeamRosterResponse getMatchTeamRosterById(@PathVariable Long id){
        return matchTeamRosterService.getMatchTeamRosterById(id);
    }
    @GetMapping("/{id}/substitution")
    public List<SubstitutionResponse> getAllMatchTeamRosterSubstitutions(@PathVariable Long id){
        return matchTeamRosterService.getAllMatchTeamRosterSubstitutions(id);
    }
    @PostMapping("/{id}/substitution")
    public SubstitutionResponse addSubstitutionToMatchTeamRoster(@PathVariable Long id, @RequestBody CreateSubstitutionRequest createSubstitutionRequest) {
        return matchTeamRosterService.addSubstitutionToMatchTeamRoster(id, createSubstitutionRequest);
    }
}
