package tn.iit.request;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class CreateSubstitutionRequest {
    private Long playerIn;
    private Long playerOut;
    private String substitutionTime;
}
