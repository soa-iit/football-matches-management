package tn.iit.request;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Setter
@Getter
public class CreateMatchRequest {
    private Long hostTeamId;
    private Long visitorTeamId;
    private LocalDate matchDate;
    private Long totalSpectators;
}
