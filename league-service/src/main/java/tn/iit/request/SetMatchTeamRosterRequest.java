package tn.iit.request;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Setter
@Getter
public class SetMatchTeamRosterRequest {
    List<Long> playerIds;
}
