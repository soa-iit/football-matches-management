package tn.iit.request;

import lombok.Getter;
import lombok.Setter;

import java.time.Year;

@Setter
@Getter
public class CreateSeasonRequest {
    private Year startYear;
    private Year endYear;
}
