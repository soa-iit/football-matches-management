package tn.iit.request;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class CreateGoalRequest {
    private Long playerId;
    private boolean ownGoal;
    private String goalTime;
}
