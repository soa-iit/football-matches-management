package tn.iit.response;

import lombok.Getter;
import lombok.Setter;
import tn.iit.entity.MatchDay;
import tn.iit.entity.Season;

import java.util.List;

@Setter
@Getter
public class SeasonResponse {
    private Long id;
    private String seasonYear;
    private List<MatchDay> matchDayList;
    public SeasonResponse(Season season) {
        this.id = season.getId();
        this.seasonYear = season.getStartYear().toString() + "/" + season.getEndYear().toString();
        this.matchDayList = season.getMatchDays();
    }
}
