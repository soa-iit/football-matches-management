package tn.iit.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
public class RankingResponse {
    private Long teamId;
    private String teamName;
    private int points;

    public RankingResponse(TeamResponse teamResponse, int points){
        this.teamId = teamResponse.getId();
        this.teamName = teamResponse.getName();
        this.points = points;
    }
}
