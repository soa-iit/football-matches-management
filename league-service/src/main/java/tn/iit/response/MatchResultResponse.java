package tn.iit.response;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class MatchResultResponse {
    private String hostTeam;
    private String visitorTeam;
    private int hostTeamGoals;
    private int visitorTeamGoals;

    public MatchResultResponse(TeamResponse hostTeamResponse, TeamResponse visitorTeamResponse, int hostTeamGoals, int visitorTeamGoals) {
        this.hostTeam = hostTeamResponse.getName();
        this.visitorTeam = visitorTeamResponse.getName();
        this.hostTeamGoals = hostTeamGoals;
        this.visitorTeamGoals = visitorTeamGoals;
    }
}
