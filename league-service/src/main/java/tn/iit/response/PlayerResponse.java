package tn.iit.response;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import tn.iit.entity.Player;
import tn.iit.entity.Position;
import tn.iit.entity.Team;

@Setter
@Getter
@NoArgsConstructor
public class PlayerResponse {
    private Long id;
    private String fullName;
    private String nationality;
    private Position position;
    private Team team;

    public PlayerResponse(Player player) {
        this.id = player.getId();
        this.fullName = player.getFirstName() + " " + player.getLastName();
        this.nationality = player.getNationality();
        this.position = player.getPosition();
        this.team = player.getTeam();
    }
}
