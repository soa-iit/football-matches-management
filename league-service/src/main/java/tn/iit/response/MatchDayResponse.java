package tn.iit.response;

import lombok.Getter;
import lombok.Setter;
import tn.iit.entity.Match;
import tn.iit.entity.MatchDay;
import tn.iit.entity.Season;

import java.util.List;

@Setter
@Getter
public class MatchDayResponse {
    private Long id;
    private int matchDayNumber;
    private Season season;
    private List<Match> matchList;

    public MatchDayResponse(MatchDay matchDay) {
        this.id = matchDay.getId();
        this.matchDayNumber = getMatchDayNumber();
        this.season = matchDay.getSeason();
        this.matchList = matchDay.getMatches();
    }
}
