package tn.iit.response;

import lombok.Getter;
import lombok.Setter;
import tn.iit.entity.MatchTeamRoster;
import tn.iit.entity.Substitution;

import java.util.List;

@Setter
@Getter
public class MatchTeamRosterResponse {
    private Long id;
    private Long teamId;
    private List<PlayerResponse> players;
    private List<Substitution> substitutions;

    public MatchTeamRosterResponse(MatchTeamRoster matchTeamRoster, List<PlayerResponse> players){
        this.id = matchTeamRoster.getTeamId();
        this.teamId = matchTeamRoster.getTeamId();
        this.players = players;
        this.substitutions = matchTeamRoster.getSubstitutions();
    }
}
