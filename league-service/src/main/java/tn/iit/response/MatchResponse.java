package tn.iit.response;

import lombok.Getter;
import lombok.Setter;
import tn.iit.entity.*;

import java.util.List;

@Setter
@Getter
public class MatchResponse {
    private Long id;
    private String hostTeam;
    private String visitorTeam;
    private Referee referee;
    private Long totalSpectators;
    private MatchTeamRoster hostTeamRoster;
    private MatchTeamRoster visitorTeamRoster;
    private List<Goal> goals;


    public MatchResponse(Match match, TeamResponse hostTeamResponse, TeamResponse visitorTeamResponse) {
        this.id = match.getId();
        this.hostTeam = hostTeamResponse.getName();
        this.visitorTeam = visitorTeamResponse.getName();
        this.referee = match.getReferee();
        this.totalSpectators = match.getTotalSpectators();
        this.hostTeamRoster = match.getHostTeamRoster();
        this.visitorTeamRoster = match.getVisitorTeamRoster();
        this.goals = match.getGoals();
    }
}
