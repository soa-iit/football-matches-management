package tn.iit.response;

import lombok.Getter;
import lombok.Setter;
import tn.iit.entity.Match;
import tn.iit.entity.MatchDay;
import tn.iit.entity.Referee;
import tn.iit.entity.Season;

import java.util.List;

@Setter
@Getter
public class RefereeResponse {
    private Long id;
    private String fullName;
    private List<Match> matches;

    public RefereeResponse(Referee referee) {
        this.id = referee.getId();
        this.fullName = referee.getFirstName()+" "+referee.getLastName();
        this.matches = referee.getMatches();
    }
}
