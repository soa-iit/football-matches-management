package tn.iit.response;

import lombok.Getter;
import lombok.Setter;
import tn.iit.entity.Match;
import tn.iit.entity.MatchDay;
import tn.iit.entity.Season;
import tn.iit.entity.Substitution;

import java.util.List;

@Setter
@Getter
public class SubstitutionResponse {
    private Long id;
    private String playerInFullName;
    private String playerOutFullName;
    private String substitutionTime;

    public SubstitutionResponse(Substitution substitution, PlayerResponse playerResponseIn, PlayerResponse playerResponseOut) {
        this.id = substitution.getId();
        this.playerInFullName = playerResponseIn.getFullName();
        this.playerOutFullName = playerResponseOut.getFullName();
        this.substitutionTime = substitution.getSubstitutionTime();
    }
}
