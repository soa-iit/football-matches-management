package tn.iit.response;

import lombok.Getter;
import lombok.Setter;
import tn.iit.entity.Goal;

@Setter
@Getter
public class GoalResponse {
    private Long id;
    private PlayerResponse scorer;
    private boolean ownGoal;
    private String goalTime;

    public GoalResponse(Goal goal, PlayerResponse playerResponse){
        this.id = goal.getId();
        this.scorer = playerResponse;
        this.ownGoal = goal.isOwnGoal();
        this.goalTime = goal.getGoalTime();
    }
}
