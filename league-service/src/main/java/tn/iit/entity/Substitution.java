package tn.iit.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.*;

import java.io.Serializable;
import java.time.LocalDateTime;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@EqualsAndHashCode(onlyExplicitlyIncluded = true)

@Entity
@Table(name = "substitutions")
public class Substitution implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ToString.Exclude
    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "id_roster", nullable = false)
    private MatchTeamRoster matchTeamRoster;

    @Column(name = "substitution_time", nullable = false)
    private String substitutionTime;

    private Long playerIn;
    private Long playerOut;


    public Substitution(MatchTeamRoster matchTeamRoster, Long playerIn, Long playerOut, String substitutionTime) {
        this.matchTeamRoster = matchTeamRoster;
        this.playerIn = playerIn;
        this.playerOut = playerOut;
        this.substitutionTime = substitutionTime;
    }
}

