package tn.iit.entity;


import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.*;

import java.io.Serializable;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@EqualsAndHashCode(onlyExplicitlyIncluded = true)

@Entity
@Table(name = "match_teams_rosters")
public class MatchTeamRoster implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ToString.Exclude
    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "id_match")
    private Match match;

    @ToString.Exclude
    @JsonIgnore
    @OneToMany(mappedBy = "matchTeamRoster", cascade = CascadeType.ALL)
    private List<Substitution> substitutions;

    @Column(name="id_team", nullable = false)
    private Long teamId;

    @OneToMany(mappedBy = "matchTeamRoster", cascade = CascadeType.ALL)
    private List<PlayerId> playerIds;

}

