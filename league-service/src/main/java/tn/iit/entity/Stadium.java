package tn.iit.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import java.io.Serializable;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class Stadium implements Serializable {


    private Long id;
    private String name;
    private Long capacity;
    @ToString.Exclude
    @JsonIgnore
    private List<Team> teamList;

    public Stadium(Long capacity, String name) {
        this.capacity = capacity;
        this.name = name;
    }
}
