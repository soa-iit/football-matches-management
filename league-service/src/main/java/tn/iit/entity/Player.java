package tn.iit.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import lombok.*;

import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class Player implements Serializable {

    @JsonIgnore
    @ToString.Exclude
    Team team;
    private Long id;
    private String firstName;
    private String lastName;
    private String nationality;
    @Enumerated(EnumType.STRING)
    private Position position;

    public Player(String firstName, String lastName, String nationality, Position position) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.nationality = nationality;
        this.position = position;
    }

}