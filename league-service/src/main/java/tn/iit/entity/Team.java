package tn.iit.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import java.io.Serializable;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class Team implements Serializable {

    @ToString.Exclude
    @JsonIgnore
    Manager manager;
    @ToString.Exclude
    @JsonIgnore
    Stadium stadium;
    @ToString.Exclude
    @JsonIgnore
    private List<Player> players;
    private Long id;
    private String name;

    public Team(Long id, String name) {
        this.id = id;
        this.name = name;
    }
}
