package tn.iit.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.*;

import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@EqualsAndHashCode(onlyExplicitlyIncluded = true)

@Entity
@Table(name = "goals")
public class Goal implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ToString.Exclude
    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "id_match", nullable = false)
    private Match match;

    private Long playerId;

    private boolean ownGoal;
    private String goalTime;

    public Goal(Match match, Long playerId, boolean ownGoal) {
        this.match = match;
        this.playerId = playerId;
        this.ownGoal = ownGoal;
    }
}

